package be.janolaerts.enterprisespringopdrachten.dao;

import java.util.List;

public interface BeerDao {

    String getBeerById(int id);
    void setStock(int id, int stock);
    List<String> getBeersByAlcohol(float alcohol);
}