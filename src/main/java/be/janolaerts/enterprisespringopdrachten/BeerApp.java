package be.janolaerts.enterprisespringopdrachten;

import be.janolaerts.enterprisespringopdrachten.dao.BeerDao;
import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrderItem;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerOrderRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerRepository;
import be.janolaerts.enterprisespringopdrachten.services.BeerService;
import be.janolaerts.enterprisespringopdrachten.services.BeerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.List;

@SpringBootApplication
public class BeerApp {

    @Autowired
    @Bean
    JdbcTemplate getTemplate(DataSource ds){
        return new JdbcTemplate(ds);
    }
    // Todo: ask Ward -> App does not stop automatically

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(BeerApp.class, args);

//        Opdracht 1
//        BeerDao dao = ctx.getBean("beerDao", BeerDao.class);
//
//        // Beer with id 26
//        System.out.println(dao.getBeerById(26));
//
//        // Beers with alcohol 5
//        List<String> beers = dao.getBeersByAlcohol(5f);
//        beers.forEach(System.out::println);
//
//        // Change stock of Bosbier (id=186) to 100
//        dao.setStock(186, 100);

        // Opdracht 2
        BeerRepository beerRepository = ctx.getBean("beerRepository", BeerRepository.class);
        BeerOrderRepository orderRepository = ctx.getBean("beerOrderRepository", BeerOrderRepository.class);

//        // Beer with id 26
        System.out.println(beerRepository.getBeerById(26));
//
////        // Beers with alcohol 5
////        List<Beer> beers = beerRepository.getBeersByAlcohol(5.0f);
////        beers.forEach(System.out::println);
////
////        // Change stock of Bosbier (id=186) to 25
////        beerRepository.setStock(186, 25);
////
////        // Update name of beer id=186 to Woudbier
////        Beer beer = beerRepository.getBeerById(186);
////        beer.setName("Woudbier");
////        beerRepository.updateBeer(beer);
////
////        // Make new order
////        BeerOrder order = new BeerOrder(
////                "Order", List.of(
////                        new BeerOrderItem(beerRepository.getBeerById(186), 1),
////                        new BeerOrderItem(beerRepository.getBeerById(285), 2),
////                        new BeerOrderItem(beerRepository.getBeerById(291), 3),
////                        new BeerOrderItem(beerRepository.getBeerById(334), 4),
////                        new BeerOrderItem(beerRepository.getBeerById(1037), 5)
////        ));
////
////        orderRepository.saveOrder(order)
    }
}