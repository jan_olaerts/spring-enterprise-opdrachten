package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;

import java.util.List;

public interface BeerRepository {

    Beer getBeerById(int id);
    void setStock(int id, int stock);
    List<Beer> getBeersByAlcohol(float alcohol);
    void updateBeer(Beer beer);
}