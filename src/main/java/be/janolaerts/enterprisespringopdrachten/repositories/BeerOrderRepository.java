package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;

public interface BeerOrderRepository {

    int saveOrder(BeerOrder order);
    BeerOrder getBeerOrderById(int id);
}