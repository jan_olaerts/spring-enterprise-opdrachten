package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;

@Repository("beerOrderRepository")
public class BeerOrderRepositoryImpl implements BeerOrderRepository {

    private EntityManager em;

    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    @Transactional
    public int saveOrder(BeerOrder order) {

        try {
            em.persist(order);
            em.flush();
            return order.getId();
        } catch (Exception e) {
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @Transactional
    public BeerOrder getBeerOrderById(int id) {

        try {
            TypedQuery<BeerOrder> query = em.createNamedQuery("getBeerOrderById", BeerOrder.class);
            query.setParameter(1, id);
            return query.getSingleResult();
        } catch (Exception e) {
            throw e;
        } finally {
            em.close();
        }
    }
}