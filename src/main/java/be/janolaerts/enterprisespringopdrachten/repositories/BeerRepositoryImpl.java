package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidBeerException;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository("beerRepository")
public class BeerRepositoryImpl implements BeerRepository {

    private EntityManager em;

    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Transactional
    public Beer getBeerById(int id) {
        
        try {
            Beer beer = em.find(Beer.class, id);
            return beer;
        } catch (Exception e) {
            throw new InvalidBeerException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }

    @Transactional
    public void setStock(int id, int stock) {

        try {
            Query query = em.createNamedQuery("updateStockById");
            query.setParameter(1, stock);
            query.setParameter(2, id);
            query.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }

    @Transactional
    public List<Beer> getBeersByAlcohol(float alcohol) {

        try {
            TypedQuery<Beer> query = em.createNamedQuery("getBeersByAlcohol", Beer.class);
            query.setParameter(1, alcohol);
            return query.getResultList();
        } catch (Exception e) {
            throw new InvalidBeerException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }

    @Transactional
    public void updateBeer(Beer beer) {

        try {
            em.merge(beer);
        } catch (Exception e) {
            throw e;
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }
}