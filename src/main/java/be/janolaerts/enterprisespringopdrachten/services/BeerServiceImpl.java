package be.janolaerts.enterprisespringopdrachten.services;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrderItem;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidBeerException;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidAmountException;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerOrderRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {

    private BeerRepository beerRepository;
    private BeerOrderRepository beerOrderRepository;

    @Autowired
    public void setBeerRepository(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Autowired
    public void setBeerOrderRepository(BeerOrderRepository beerOrderRepository) {
        this.beerOrderRepository = beerOrderRepository;
    }

    @Override
    @Transactional(rollbackOn=InvalidBeerException.class)
    public int orderBeer(String name, int beerId, int amount) {
        if(amount < 0) throw new InvalidAmountException("amount cannot be smaller than 0");

        Beer beer = beerRepository.getBeerById(beerId);
        if(beer == null) throw new InvalidBeerException("Beer does not exist");

        List<BeerOrderItem> beerOrderItems = List.of(
                new BeerOrderItem(beer, amount));
        BeerOrder beerOrder = new BeerOrder(name, beerOrderItems);
        beerRepository.setStock(beerId, beer.getStock() -amount);
        return beerOrderRepository.saveOrder(beerOrder);
    }

    @Override
    @Transactional(rollbackOn=InvalidBeerException.class)
    public int orderBeers(String name, int[][] order) {
        if(order[0].length != order[1].length)
            throw new IllegalArgumentException("The number of beer ids and the number of amounts must be the same");

        Beer beer;
        int amount;
        List<BeerOrderItem> beerOrderItems = new ArrayList<>();

        for(int i = 0; i < order[0].length; i++) {

            beer = beerRepository.getBeerById(order[0][i]);
            if(beer == null) throw new InvalidBeerException("Beer does not exist");

            amount = order[1][i];
            if(amount < 0) throw new InvalidAmountException("amount cannot be smaller than 0");

            BeerOrderItem boi = new BeerOrderItem(beer, amount);
            beerOrderItems.add(boi);
            beerRepository.setStock(beer.getId(), beer.getStock() -amount);
        }

        BeerOrder beerOrder = new BeerOrder(name, beerOrderItems);
        return beerOrderRepository.saveOrder(beerOrder);
    }
}