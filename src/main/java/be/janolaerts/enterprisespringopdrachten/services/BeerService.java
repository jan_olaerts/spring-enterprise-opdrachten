package be.janolaerts.enterprisespringopdrachten.services;

import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidBeerException;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidAmountException;

public interface BeerService {

    int orderBeer(String name, int beerId, int number) throws InvalidBeerException, InvalidAmountException;
    int orderBeers(String name, int[][] order) throws InvalidAmountException, InvalidBeerException;
}