package be.janolaerts.enterprisespringopdrachten.entity;

import javax.persistence.*;
import java.util.Arrays;

@Entity
@Table(name = "Beers")
@NamedQueries(value={
        @NamedQuery(name="updateStockById", query="update Beer set stock=?1 where id=?2"),
        @NamedQuery(name="getBeersByAlcohol", query="select b from Beer b where b.alcohol=?1")
})
public class Beer {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "Name")
    private String name;

    @JoinColumn(name = "BrewerId")
    @ManyToOne
    private Brewer brewer;

    @JoinColumn(name = "CategoryId")
    @ManyToOne
    private Category category;

    @Column(name = "Price")
    private double price;

    @Column(name = "Stock")
    private int stock;

    @Column(name = "Alcohol")
    private float alcohol;

    @Version
    @Column(name = "Version")
    private int version;

    @Lob
    @Column(name = "Image")
    private byte[] image;

    public Beer() {
    }

    public Beer(String name, double price, int stock, float alcohol) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.alcohol = alcohol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brewer getBrewer() {
        return brewer;
    }

    public void setBrewer(Brewer brewer) {
        this.brewer = brewer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(float alcohol) {
        this.alcohol = alcohol;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brewer=" + brewer +
                ", category=" + category +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                ", version=" + version +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}