package be.janolaerts.enterprisespringopdrachten.dao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BeerDaoTest {

    @Autowired
    private BeerDao dao;

    @Test
    public void testGetBeerById() {
        String beer = dao.getBeerById(1);
        assertEquals("TestBeer 7.0 2.75 100", beer);
    }

    @Test
    public void testSetStock() {
        dao.setStock(1, 50);
        String beer = dao.getBeerById(1);
        assertEquals("TestBeer 7.0 2.75 50", beer);
    }

    @Test
    public void testGetBeersByAlcohol() {
        List<String> beers = dao.getBeersByAlcohol(7.0f);
        assertEquals("[{NAME=TestBeer, ALCOHOL=7.0, PRICE=2.75, STOCK=100}]", beers.toString());
    }
}