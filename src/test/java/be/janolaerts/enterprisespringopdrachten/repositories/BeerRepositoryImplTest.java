package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql(scripts = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/schema.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class BeerRepositoryImplTest {

    @Autowired
    private BeerRepository beerRepository;

    @Test
    void getBeerById() {
        Beer beer = beerRepository.getBeerById(1);
        assertEquals("TestBeer", beer.getName());
    }

    @Test
    void setStock() {
        Beer beer;

        beer = beerRepository.getBeerById(1);
        beerRepository.setStock(beer.getId(), 512);

        beer = beerRepository.getBeerById(1);
        assertEquals(512, beer.getStock());
    }

    @Test
    void getBeersByAlcohol() {
        List<Beer> beers;

        beers = beerRepository.getBeersByAlcohol(3f);
        assertEquals("[]", beers.toString());

        beers = beerRepository.getBeersByAlcohol(7f);
        assertEquals(1, beers.size());
    }

    @Test
    void updateBeer() {
        Beer beer;

        beer = new Beer("UpdatedTestBeer", 250, 519, 5.3f);
        beer.setId(1);
        beerRepository.updateBeer(beer);

        beer = beerRepository.getBeerById(1);
        assertEquals("UpdatedTestBeer", beer.getName());
        assertEquals(250, beer.getPrice());
        assertEquals(519, beer.getStock());
        assertEquals(5.3f, beer.getAlcohol());
    }
}