package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrderItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql(scripts = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/schema.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class BeerOrderRepositoryImplTest {

    @Autowired
    private BeerRepository beerRepository;

    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @Test
    void saveOrder() {
        BeerOrder order;

        order = new BeerOrder(
                "Order 3", List.of(
                new BeerOrderItem(beerRepository.getBeerById(186), 1),
                new BeerOrderItem(beerRepository.getBeerById(285), 2),
                new BeerOrderItem(beerRepository.getBeerById(291), 3),
                new BeerOrderItem(beerRepository.getBeerById(334), 4),
                new BeerOrderItem(beerRepository.getBeerById(1037), 5)
        ));
        beerOrderRepository.saveOrder(order);

        order = beerOrderRepository.getBeerOrderById(3);
        assertEquals(3, order.getId());
        assertEquals("Order 3", order.getName());
        assertEquals("BeerOrder{id=3, name='Order 3', items=[BeerOrderItem{id=7, beer=null, number=1}, " +
                "BeerOrderItem{id=8, beer=null, number=2}, BeerOrderItem{id=9, beer=null, number=3}, " +
                "BeerOrderItem{id=10, beer=null, number=4}, BeerOrderItem{id=11, beer=null, number=5}]}",
                order.toString());
    }

    @Test
    void getBeerOrderById() {
        BeerOrder order = beerOrderRepository.getBeerOrderById(2);
        assertEquals(2, order.getId());
    }
}