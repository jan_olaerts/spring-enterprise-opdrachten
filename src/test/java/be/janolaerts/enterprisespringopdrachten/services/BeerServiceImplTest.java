package be.janolaerts.enterprisespringopdrachten.services;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidBeerException;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidAmountException;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerOrderRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql(scripts = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/schema.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
class BeerServiceImplTest {

    // Todo: ask Ward: Test ok but one fails with mvn

    @Autowired
    private BeerRepository beerRepository;

    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @Autowired
    BeerService beerService;

    @Test
    void orderBeer() {
        assertThrows(InvalidBeerException.class, () -> beerService.orderBeer("Order", 1037, 6));
        assertThrows(InvalidAmountException.class, () -> beerService.orderBeer("Order", 2, -1));

        int orderId = beerService.orderBeer("Order", 2, 5);
        assertEquals(3, orderId);
    }

    @Test
    void orderBeers() {
        assertThrows(InvalidBeerException.class, () -> beerService.orderBeers("Order", new int[][] {{1, 4}, {1, 6}}));
        assertThrows(InvalidAmountException.class, () -> beerService.orderBeers("Order", new int[][] {{1, 3}, {5, -1}}));
        assertThrows(IllegalArgumentException.class, () -> beerService.orderBeers("Order", new int[][] {{1, 3}, {5}}));

        int[][] order = new int[][] {{1, 2, 3}, {3, 2, 1}};
        int orderId = beerService.orderBeers("Order", order);
        BeerOrder beerOrder = beerOrderRepository.getBeerOrderById(4);
        assertEquals(4, orderId);
        assertEquals(1, beerOrder.getItems().get(2).getNumber());
    }
}